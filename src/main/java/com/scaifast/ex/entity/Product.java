package com.scaifast.ex.entity;

import lombok.Builder;
import lombok.Data;

// @Entity
@Data
@Builder
public class Product {

    private Integer id;
    private String uuid;
    private String name;
    private String description;
    private Integer categoryId;
    private Image image;
}
