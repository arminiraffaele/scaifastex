package com.scaifast.ex.service.mapper;

import com.scaifast.ex.entity.Product;
import com.scaifast.ex.web.data.ProductDto;
import org.springframework.stereotype.Service;

@Service
public class ProductMapper {

    public ProductDto toProductDto(Product product) {
        return new ProductDto(product.getUuid(), product.getName(), product.getDescription(), product.getCategoryId(), product.getImage());
    }

}
