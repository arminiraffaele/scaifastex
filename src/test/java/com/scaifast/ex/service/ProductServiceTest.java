package com.scaifast.ex.service;

import com.scaifast.ex.service.mapper.ProductMapper;
import com.scaifast.ex.web.data.ProductDto;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    private ProductService productService;

    @BeforeEach
    public void setUp() {
        productService = new ProductService(new ProductMapper());
    }

    @Test
    public void shouldReturnProductAll() {
        // should mock repository but It does not exist (no database configured)
        List<ProductDto> productsDto = productService.findAllOrByCategoryId(null);
        Assertions.assertThat(productsDto).isNotNull();
        Assertions.assertThat(productsDto).isNotEmpty();
    }

    @Test
    public void shouldReturnProductByCategoryId() {
        // should mock repository but It does not exist (no database configured)
        List<ProductDto> productsDto = productService.findAllOrByCategoryId(2);
        Assertions.assertThat(productsDto).hasSize(1);
        Assertions.assertThat(productsDto.get(0).getCategoryId()).isEqualTo(2);
    }
}
