package com.scaifast.ex.web.rest;

import com.scaifast.ex.service.ProductService;
import com.scaifast.ex.web.data.ProductDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.constraints.Min;
import java.util.List;

@RequestMapping("/api/v1")
@RestController
@AllArgsConstructor
@Validated
public class ProductsApiResource {

    private ProductService productService;

    @ApiOperation(value = "Get products", nickname = "getProducts", response = ProductDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Returned when successful", response = ProductDto.class),
            @ApiResponse(code = 401, message = "Returned when not authorized"),
            @ApiResponse(code = 404, message = "Returned when Product Not Found"),
            @ApiResponse(code = 503, message = "Returned when the service is unavailable"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    @GetMapping("/products")
    public ResponseEntity<List<ProductDto>> getProducts(
            @RequestParam(required = false) @Min(0) Integer categoryId) {
        return ResponseEntity.ok().body(productService.findAllOrByCategoryId(categoryId));
    }


    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<String> handleConstraintViolationException(ConstraintViolationException e) {
        return new ResponseEntity<>("validation error: " + e.getMessage(), HttpStatus.BAD_REQUEST);
    }

}
