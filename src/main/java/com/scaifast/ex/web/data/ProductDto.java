package com.scaifast.ex.web.data;

import com.scaifast.ex.entity.Image;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ProductDto {
    private String uuid;
    private String name;
    private String description;
    private Integer categoryId;
    private Image image;
}
