package com.scaifast.ex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScaifastApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScaifastApiApplication.class, args);
    }

}
