package com.scaifast.ex.entity;

import lombok.Data;

@Data
public class Image {
    private String src;
}
