package com.scaifast.ex.service;

import com.scaifast.ex.entity.Product;
import com.scaifast.ex.service.mapper.ProductMapper;
import com.scaifast.ex.web.data.ProductDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ProductService {

    // repository
    // private final ProductRepository productRepository;

    private List<Product> productsData = new LinkedList<>();
    private final ProductMapper productMapper;

    public ProductService(ProductMapper productMapper) {
        this.productMapper = productMapper;

        // fill fake data
        productsData.add(Product.builder()
                .id(1)
                .uuid(UUID.randomUUID().toString())
                .name("Synergistic Granite Pants")
                .description("Synergistic Granite Pants")
                .categoryId(1)
                .build());

        productsData.add(Product.builder()
                .id(2)
                .uuid(UUID.randomUUID().toString())
                .name("Incredible Plastic Lamp")
                .description("Incredible Plastic Lamp ... description")
                .categoryId(2)
                .build());

        productsData.add(Product.builder()
                .id(3)
                .uuid(UUID.randomUUID().toString())
                .name("Product 3")
                .description("Product 3 description")
                .categoryId(1)
                .build());
    }

    public List<ProductDto> findAllOrByCategoryId(Integer categoryId) {
        if (categoryId == null || categoryId < 0) {
            return productsData.stream().map(productMapper::toProductDto).collect(Collectors.toList());
        } else {
            return productsData.stream().filter(product -> product.getCategoryId() == categoryId)
                    .map(productMapper::toProductDto).collect(Collectors.toList());
        }
    }
}
