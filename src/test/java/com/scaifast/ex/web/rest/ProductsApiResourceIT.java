package com.scaifast.ex.web.rest;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class ProductsApiResourceIT {

    @Autowired
    protected MockMvc mockMvc;

    final private String baseApiUrl = "/api/v1/products";

    @Test
    void shouldReturnAllProducts() throws Exception {
        this.mockMvc
                .perform(get(baseApiUrl)
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.[0].categoryId").value(1))
                .andExpect(jsonPath("$.[1].categoryId").value(2))
                .andExpect(jsonPath("$.[2].categoryId").value(1));
    }

    @Test
    void shouldReturnProductsByCategoryId() throws Exception {
        this.mockMvc
                .perform(get(baseApiUrl + "?categoryId=2")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.[0].categoryId").value(2));
    }

    @Test
    void shouldReturnBadRequestOnCategoryIdNegative() throws Exception {
        this.mockMvc
                .perform(get(baseApiUrl + "?categoryId=-2")
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().is4xxClientError());
    }
}
